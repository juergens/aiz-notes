## do whatver ever you want

If you have a quick fix, just make it on master. The CI will tell you if it works.

If you found a error, but don't have fix, or if you don't understand something, just open an issue here.

You can submit changs via merge-request; or you can request write-access; or you can open an issue and comment with you fix. I don't care, as long as the fix arrives here somehow.

## builing locally

For bigger changes I recommend to clone the repo and compiling it locally (`make pdf`).

you can auto-update with this simple bash on-liner (assuming ubuntu 18.04):

    evince build/example.pdf &; while inotifywait -e close_write source; do make pdf; done

This rebuilds the repo whenever you save a file, and it opens evince on the output, which refreshes whenever the pdf changes.
