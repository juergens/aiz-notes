
# Load in new config settings
include _CONFIG.txt

# This combines all the filepaths in SECTIONS_FILEPATH file
SECTIONS := $(shell cat _SECTIONS.txt | tr '\n\r' ' ' | tr '\n' ' ' )

TEMPLATE_DIR=$(shell realpath $(shell dirname $(TEMPLATE)))

PROJECT_PATH=$(shell realpath .)

PRE_CMD=mkdir -p build && cd $(PROJECT_PATH)/source/

BASE_CMD=pandoc --toc --bibliography=$(REFERENCES)  --csl=$(PROJECT_PATH)/csl/$(CSL).csl --standalone --self-contained --toc -N  --bibliography=$(REFERENCES) --reference-links --reference-location section -V links-as-notes

WEB_CMD=$(BASE_CMD) --webtex --section-divs --reference-links

TEX_CMD=TEXINPUTS=$(TEXINPUTS):$(TEMPLATE_DIR) $(BASE_CMD) --template=$(PROJECT_PATH)/$(TEMPLATE) --top-level-division=chapter --pdf-engine=xelatex

FILES=$(shell find source/ -type f -name '*'|sed 's/ /\\ /g')

OUT=$(PROJECT_PATH)/build/$(BUILDNAME)

.PHONY: clean
default: pdf
.DEFAULT_GOAL := pdf

all: pdf tex html mobi

clean:
	rm -rf build


# build the document
$(OUT).pdf: source $(FILES)
		$(PRE_CMD) && \
		$(TEX_CMD) -o $(OUT).pdf $(SECTIONS)

pdf: $(OUT).pdf

$(OUT).tex: source $(FILES)
		$(PRE_CMD) && \
		$(TEX_CMD) -o $(OUT).tex $(SECTIONS)

tex: $(OUT).tex


# build document as html
$(OUT).html: source $(FILES)
		$(PRE_CMD) && \
		$(WEB_CMD) -o $(OUT).html -t html --css $(PROJECT_PATH)/$(HTML_STYLE) $(SECTIONS)

html: $(OUT).html

pres:
		$(PRE_CMD) && \
		$(WEB_CMD)-i -t revealjs -V revealjs-url=http://lab.hakim.se/reveal-js --slide-level=2 -o $(OUT).html meta.yaml introduction.md


# build as ebook
$(OUT).fb2: source $(FILES)
		$(PRE_CMD) && \
		$(WEB_CMD) -o $(OUT).fb2 $(SECTIONS)

ebook: $(OUT).fb2

$(OUT).mobi: $(OUT).fb2
		$(PRE_CMD) && \
		ebook-convert $(OUT).fb2 $(OUT).mobi > /dev/null

mobi: $(OUT).mobi


# the following are just personal convenience
reader: $(OUT).pdf
		nohup evince $(OUT).pdf &

loop:
		 while inotifywait -e close_write source; do make pdf; done

editor:
		nohup atom . &

ide: editor reader loop
