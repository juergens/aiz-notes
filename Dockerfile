FROM ubuntu:latest
LABEL cache_breaker 2018_07_25

RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y calibre build-essential

# basic latex
RUN apt-get install -y texlive  texlive-latex-extra

# local latex
RUN apt-get install -y texlive-lang-german

# custom latex
RUN apt-get install -y texlive-math-extra


# pandoc in ubuntu-repo is outdated
ENV PANDOC_URL https://github.com/jgm/pandoc/releases/download/2.2.2.1/pandoc-2.2.2.1-1-amd64.deb

RUN apt-get install -y wget sendemail texlive-xetex

RUN TEMP_DEB="$(mktemp)" && \
	wget -O "$TEMP_DEB" "$PANDOC_URL" && \
	dpkg -i "$TEMP_DEB" && \
	rm -f "$TEMP_DEB"

RUN apt-get install -y pandoc-citeproc
