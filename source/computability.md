
# Berechnungsmächtigkeit von Zellularau-tomaten

## Definitionen

\begin{deftable}
\tc Allgemeines
\\ (deterministische) Turingmaschine
&TM
& Zustandsmenge  $Q$
\newline Bandalphabet $B$
\newline Überführungsfunktion $\delta: Q \times B \to Q \times B \times \{-1, 0, 1\}$
\nt Konfiguration $c = (s, b, p) \in Q × B^\mathbb Z \times \mathbb Z$
\newline Konfigurationsüberführungsfunktion $\Delta: Q \times B^\mathbb Z \times \mathbb Z \to Q \times B^\mathbb Z \times \mathbb Z$
&
\end{deftable}

## Sätze / Fakten

- wireworld ist turing vollständig
	- Beweis: man kann Logik-Gatter bauen

- $\forall TM t: \exists 1D-CA c:$ $c$ simuliert $t$ ohne Zeitverlust schrittweies
	- Beweisidee:
		- $Q_c = \{Q_T\cup \_\} \times B_T$
		- Zustände der Zellen sind Band
		- Und aktive Zelle hat außerdem noch den Zustand
		- Überführungsfunktion wird von aktiver Zeller ausgeführt (um neuen Wert auf Band zu schreiben) und von direktem Nachbarn, falls sich Zeiger bewegt
