
# Async (extra)

## Sätze und Fakten

- Async CA:
	- Zufall entscheidet welche Zellen arbeiten
- prob. CA:
	- Zufall entschedet neuen Zustand der Zellen
	- Async CA ist Sonderfall von prob. CA

- Nakamura’s technique
	- Ziel: Baue synchrone überführungsfunktion um, sogdass sie auf async CA funktioniert.
	- Idee: Zellen dürfen max. 1 Schritt weiter sein als nachbarn
	- $Q' = Q \times Q \times \{0,1,2\}$
		- $alt \times neu \times Zeit$

## Zusammenfassung

- aynchronous CA have “no gloal clock”
	- any subset of cells can be updated in a global step
- probabilistic CA allow each cell choose between different new states (if wanted)
	- this can help to solve – with high probability – problems which are otherwise unsolvable
