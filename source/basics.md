
# Grundlegende Definitionen

\begin{deftable}
\tc Allgemeines
\\ Raum
& $R$
& $\mathbb{Z} \times ... \times  \mathbb{Z}$
\newline oder $\mathbb{Z}_n \times ... \times$
& in diesem Raum sind Zellen geordernet. Zellen werden über Koordinaten $(i_1, ... , i_d)$ adressiert
\\\hline (globale) Konfiguration
& $c: R \to Q$
&
& Schreibweise $c_i:=c(i)$ für Zustand der Zelle $i$
\\\hline & $Y^X$&$\{f \mid f: X\to Y\}$&  z.B. $c\in Q^R$
\\\hline Zustandsmenge&$Q$&&stets endlich; Rest beliebig
\\\hline Nachbarschaftsindex oder Nachbarschaft
& $N$
& $\{n_1, \dots, n_k\} \subset \mathbb Z^d$
& $\mathbb Z^d$ ist hier nicht der Raum, sondern die Differenz der Koordinaten
\\\hline lokale Konfiguration
& $l:N\to Q$
&
& Das sind die Zustände der Nachbarschaft einer Zelle
\newline $l(0):=$ Die Zelle selbst
\\\hline Moore-Nachbarschaft
& $M_r^{(d)}$
\nt $r$: Radius, i.d.R. $1$
\newline $d$: Dimension, i.d.R. $2$
& $\{ (-1,-1), ..., (1,1)\}$ \nt falls $r=1, d=2$
& Diagonale wird behandelt wie direkte Nachbarn; sind Quadratisch
\\\hline Von-Neumann-Nachbarschaft
& $H_r^(d)$
&
& Diagonale sind 2 Schritte; sind schräge Quadrate
\\\hline lokale Überführungsfunktion
& $\delta$
& $Q^N \to Q$
\nt $Q^N$ sind alle Abb. von $N$ nach $Q$
& Die Zustände der Nachbarschaft werden auf einen neuen Zustand abgebildet. Die Reihenfolge der Nachbarn ist i.A. z beachten
\\\hline lokale Konfiguration
& $c_{i+N}: N \to Q$
& $n \mapsto c_{i+n}$
&
\\\hline globale Überführungsfunktion
& $\Delta Q^R \to Q^R$
& $\forall i\in R: $ $\Delta(c)_i $
\newline $= (\Delta(c))_i$
\newline $= \delta(c_i+N)$
&

\\\hline \tc wichtige Automaten
\\ wireworld&\mct{
Schwarz --> Schwarz
\newline Rot --> Blau
\newline Weiß --> Rot (Falls 1 oder 2 Nachbarn Rot), Sonst weiß
}
\\\hline life & \mct{
$switch(\sum(N))$
\newline case $\leq 2: 0$
\newline case $= 3: 1$
\newline case $= 4: l(0)$
\newline case $\geq 5: 0$
}
\\\hline banks &&&
\end{deftable}
