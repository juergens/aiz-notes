
# Sortiern in 1D

- (ODD-EVEN-TRANSPOSITION-SORT)
- 0-1-Sortierlemma von Knuth
	- Wenn ein Sortieralgorithmus ausschließlich aus Operationen der Art „Vergleich-und-Austausch-wenn-größer“ besteht und wenn von vorneherein (unabhängig von den zu sortierenden Daten) feststeht, an welchen Positionen Werte miteinander verglichen und gegebenenfalls vertauscht werden, dann gilt: Der Algorith- mus sortiert genau dann alle Eingabedatensätze, wenn er alle Eingabedatensätze sortiert, die nur aus Nullen und Einsen bestehen.

## Zusammenfassung
Knuths 0-1-Sortierlemma sichert zu, dass bei gewissen Sortieralgorithmen für den Nachweis der Korrektheit die Betrachtung von 0-1-Eingaben genügt.

Sortieren in eindimensionalen Zellularautomaten mit einem Datum je Zelle ist in Linearzeit möglich.
