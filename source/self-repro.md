
# Selbstreproduktion

## Defintionen

\begin{deftable}
\tc Allgemein
\\ &&&
\\\hline &&&
\end{deftable}

## Sätze und Fakten

- triviale Selbstreproduktion: Reproduziere einsen

- fragen von von Neumann
	- A
	- B
	- C
	- D
	- E
- Idee des automaten mit 29 Zuständen

## Zusammenfassung

Es gibt sehr einfache Zellularautomaten, in denen sich für alle endlichen Anfangskonfigurationen der Träger nach endlichen vielen Schritten selbst reproduziert.

Von Neumann beschrieb als erster einen Zellularautomaten, der berechnungs- und konstruktionsuniversell ist. Dabei ist die Bezeichnung „konstruktionsuniversell“ zugegebenermaßen nicht präzise gefasst worden und wohl zu viel versprechend. Die Beispiele sollten aber klar gemacht haben, was gemeint ist.
