
# Einige grundlegende Techniken für Zellularau-tomaten

## Def

\begin{deftable}
\tc abc
\\  Signal &&&
\\\hline (ZA-)konstruierbares Signal &&&
\\\hline (ZA-)konstruierbare Markierung &&&
\\\hline Ankunftszeit \nt von Signal
&
&
&

\\\hline
\tc Zähler und einfache Arithmetik
\\ &&&
\\\hline &&&
\\\hline &&&
\\\hline &&&
\end{deftable}

## Sätze und Fakten

## Zusammenfassung


- Für jede rationale Zahl r, die kleiner oder gleich dem „Radius“ der Nachbarschaft ist, kann man Signale konstruieren, deren Durchschnittsgeschwindigkeit gleich r ist.
- Lässt man solche Signale geschickt zusammenwirken, dann kann man auch kompliziertere Signale realisieren.
- Häufig verwendet man Signale, um bestimmte Zellen oder/und Zeitpunkte zu markieren.
- Nichtnegative ganze Zahlen kann man speichern, indem man die Bits ihrer Dualzahldar- stellung in nebeneinanderliegenden Zellen ablegt.
- Solche Zahlen kann man wandern lassen, und dabei zum Beispiel bei Vorliegen lokal ent- scheidbarer Kriterien de- oder inkrementieren.
- Mit solchen Zahldarstellungen kann man auch (schnell) einfache Arithmetik betreiben.
