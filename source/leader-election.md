
# leader election (extra)

## Def

\begin{deftable}
\tc abc
\\ &&&
\\\hline &&&
\end{deftable}

## Sätze und Fakten

## Zusammenfassung

- In d-dimensionalen Zellularautomaten kann man das Leader-Election-Problem in beliebigen Mustern (auch mit Löchern) in Zeit ($\Theta(diam * log(diam))$) lösen.
- Für Muster ohne Löcher kann man mit Zeit ($\Theta(diam)$) auskommen (Nichitiu, Papazian und
Rémila 2004).
- Die beste bekannte untere Schranke für den Zeitbedarf $O(diam)$. Es ist ein nach wie offenes Problem, ob man dies nach oben verbessern kann oder Laufzeiten von Algorithmen nach unten.
