
# Sortieren in 2 D

## Def

\begin{deftable}
\tc abc
\\ &&&
\\\hline &&&
\end{deftable}

## Sätze und Fakten

- shearsort
	- in Ungeraden Phasen: sortiere in den Zeilen
	- in geraden Phasen: sortiere in Spalten
	- Anzahl Phasen: $1+2*\lceil \log m\rceil$ 
- Schnorr und Shamir
	- $\Theta(\sqrt(n))$
	- ![Schnorr und Shamir](assets/markdown-img-paste-20180930123745923.png)

## Zusammenfassung

- In eindimensionalen Zellularautomaten kann man n Datenelemente in Zeit $O(n)$ sortieren.
- Im Zweidimensionalen ist die Schlangenlinie eine sinnvolle Sortierreihenfolge, weil in der Sortierung benachbarte Datenelemente am Ende auch in benachbarten Zellen liegen sollen.
- Sortieren von $\sqrt n\times \sqrt n$ Elementen in Schlangenlinienform ist in Zeit $O(\sqrt n)$ möglich.
- Dabei ist $3 \sqrt n - o( \sqrt n)$ untere Zeitschranke, sofern man beliebig viele verschiedene Werte
sortieren können will.
