
# Einführung

Motivation

Es gibt viele praktische Anwendungen von ZA:  https://www.researchgate.net/publication/270451971_A_Survey_on_Cellular_Automata_and_Its_Applications

* Simulation
	* Physik
	* Biologie
* Graph-Algorithmen mit lokaler Information

Techniken und Denkweisen von ZA werden für Selbstorganiserung verwendet (Apache Cassandra, Router, p2p Netze, ...), auch wenn hier nicht explizit von ZA gesprochen wird, lassen sich doch viele Ähnlichkeiten erkennen. Inbesondere ermölichen ZA Skalierung in einem Maße, in dem eine zentrale Verwaltung nicht mehr möglich ist.

Es gibt Ansätze, um mit ZA aktuelle CMOS-Technologie abzulösen: https://en.wikipedia.org/wiki/Quantum_dot_cellular_automaton

Es gibt Versuche das Universum mithilfe von ZA zu erklären: https://en.wikipedia.org/wiki/A_New_Kind_of_Science
