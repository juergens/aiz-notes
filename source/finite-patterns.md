
# Endliche Muster und Konfigurationen

## Definitionen

\begin{deftable}
\tc Allgemein
\\ passive Menge &&&
\\\hline Ruhemenge &&siehe \textit{passive Menge}&
\\\hline Ruhezustand &&&
\\\hline toter Zustand&&&
\\\hline Muster &&&
\\\hline Träger &&&
\\\hline endliche Konfiguration&&Konfiguration deren Träger endlich ist&
\\\hline akzeptierende Endkonfiguration &&&
\\\hline ablehnende Endkonfiguration &&Endkonfiguration, die nicht akzeptiert&
\\\hline &&&
\\\hline &&&
\\\hline &&&
\end{deftable}

## Sätze und Fakten

## Zusammenfassung

Endliche Konfigurationen sind Konfigurationen, in denen bis auf endlich viele Ausnahmen alle Zellen in einem ausgezeichneten Ruhezustand sind.

„Ausschnitte“ endlich vieler Zellen aus R mit der Festlegung ihrer Zustände heißen Muster. Durch Auffüllen mit Ruhezuständen erhält man die zugehörigen Musterkonfigurationen.

Bei der Erkennung formaler Sprachen erfolgt die Eingabe eines Wortes w durch Wahl der zu $(i, 0, \dots , 0) \mapsto w_i$ gehörenden Musterkonfiguration als Anfangskonfiguration. Die Entscheidung über Annahme oder Ablehnung von w wird anhand des Zustandes von Zelle $(1, 0, \dots , 0)$ gefällt.

Bei Beschränkung auf endliche Konfigurationen (wie z. B. bei der Erkennung formaler Sprachen) sind Turingmaschinen und Zellularautomaten äquivalent.
