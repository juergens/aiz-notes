# general parameters
BUILDNAME=document
CSL=elsevier-with-titles


# parameters for pdf/tex-document
REFERENCES=references.bib
TEMPLATE=template/latex-generic/template.tex

# parameters for html-document
HTML_STYLE=template/html-generic/style.css

# parameters for presentation
PRESENTATION=presentation/example.md
MATHJAX_URL="http://cdn.mathjax.org/mathjax/latest/MathJax.js"
REVEALJS_URL=reveal.js
