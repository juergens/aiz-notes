
# Notizen zu _Algorithem in Zellularautomaten_

Use this to write a paper in Markdown and render it as PDF, HTML, mobi or epub.

You can build this manually (via make) or via CI.

You can find examples of the compiled documents  [here](https://gitlab.com/juergens/aiz-notes/builds/artifacts/master/browse/build/?job=build)

forked from https://github.com/juergens/markdown-latex-boilerplate

## running the build

	make all

After this the compiled document can be found in folder `build`

For more build targets, see `Makefile`

If you don't want a presentation or a HTML-Document or a PDF-Document, remove it from `_CONFIG.txt` and from the corresponding-job from the makefile


## CI

If you don't want CI, remove `.gitlab-ci.yml` from the project

If you host your project on gitlab, CI should work automagically. You just need to push your commits, and 2 minutes later the artefacts can be downloaded.

After cloning your repo, you need to define a runner to get going:

get token from: Setting-->
CI / CD Settings --> Runners --> Specific Runners --> Setup a specific Runner manually

Configure Runner with token from above:

	docker run --rm -t -i -v /srv/gitlab-runner/config:/etc/gitlab-runner --name gitlab-runner-aiz gitlab/gitlab-runner register

start runner

	docker run -d --restart always --name gitlab-runner-aiz -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

view runner's logs

	docker logs gitlab-runner-aiz

### upload to kindle

If you don't want to upload to a kindle, you should remove the "send_mobi" job from `.gitlab-ci.yml`.

If you do want it, you need to add email-credentials to the project:

- create dummy-account on gmail
- allow that account to upload to your kindle
- enter variables: gitlab-->settings-->ci/cd/-->Variables
	- Note: All of these **must** be protected
	- SMTPUSER
	- SMTPPASS
	- SMTPSERVER
	- SMTPFROM
	- SMTPTO
	- TEST
		- this is a debug-variables, that gets printed in plaintext, so you can in the job-output, if protected vars arrived at the job.
- create branch "deploy"
- make branch `deploy` protected
	- gitlab-->settings-->repository-->protected branches
- make tabs `-*deploy` protected
	- gitlab-->settings-->repository-->protected tags

Note: Protected variables are available on jobs, that run on a protected branch or tag. Make sure no secrets leak to log file or untrusted commits

## Tips and Tricks

### auto build and preview

	evince build/example.pdf & while inotifywait -e close_write source; do make pdf; done

What's happening here?

`evince` is a pdf reader, that automatically refreshes, when the document changes. `&` will push evince to the background. `inotifywait` is a program, that stops, when a file is changed, and `make pdf` rebuilds the pdf


### atom texteditor

* use `language-markdown` and not the default `language-gfm`. ([reason](https://github.com/atom/language-gfm/issues/117#issuecomment-159977414))
* use `linter-markdown`, `minimap`, `git-log`

### windows

[the upstream repository](https://github.com/davecap/markdown-latex-boilerplate) was focusing on windows-user and contains a lot of useful tipps
